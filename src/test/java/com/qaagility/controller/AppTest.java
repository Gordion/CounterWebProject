package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import com.qaagility.controller.Calculator;
 

public class AppTest {

Calculator myCalc = new Calculator();

	@Test	
      public void testSlow() {
        System.out.println("slow");
      }

	@Test	
      public void testSlower() {
        System.out.println("slower");
      }

	@Test	
      public void testFast() {
        System.out.println("fast-updated");
      }
	
	@Test	
      public void testCalc() {
        assertEquals("Result", 9, myCalc.add());
      }

    }
